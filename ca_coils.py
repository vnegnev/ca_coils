#!/usr/bin/python3

from pylab import *

# Simulates the Biot-Savart law for a loop of wire, with a stub halfway.

# Geometry shown in coil_geom.png .

# Basic parameters for simulation

# loop radius, metres
loop_r = 0.2 
# end stub length, breaking up half-wave loop segments. Since wire
# goes out by this distance then back, total phase delay is twice the
# length.
end_stub_l = 0 
# wavelength of EM waves along wire
wavelength = 1000
# segments along whole loop
n_segs = 20
# current in loop (peak)
i_pk = 1

# Ion position, with centre of the coil as the origin
ion_x = 0.0
ion_y = 0.0
ion_z = -0.5

# Loop vectors, as defined by the coil geometry
# theta: angle along the loop circumference
def loop_coord(theta):
    x = loop_r * cos(theta)
    y = loop_r * sin(theta)
    z = zeros(size(theta))
    return x,y,z

k = arange(0,n_segs)
theta = arange(0,2*pi,2*pi/n_segs)
wire_segs_x, wire_segs_y, wire_segs_z = loop_coord(theta)
wire_segs_wave_theta = theta * loop_r / wavelength

# Current vectors in wire (current flows anticlockwise through loop, looking down)
i_x, i_y, i_z = -i_pk*sin(theta)*cos(wire_segs_wave_theta), \
                i_pk*cos(theta)*cos(wire_segs_wave_theta), \
                i_pk*0*cos(wire_segs_wave_theta)

# Figure out r vectors from each loop segment to ion
r_x, r_y, r_z = ion_x - wire_segs_x, ion_y - wire_segs_y, ion_z - wire_segs_z

# calculating B
def cross_prod(x1,y1,z1,x2,y2,z2):
    return y1*z2-z1*y2, z1*x2 - x1*z2, x1*y2 - y1*x2
def test_cross_prod():
    assert cross_prod(1,0,0,0,1,0) == (0,0,1)
    assert cross_prod(0,1,0,1,0,0) == (0,0,-1)
    assert cross_prod(0,1,0,0,0,1) == (1,0,0)
    assert cross_prod(0,0,1,0,1,0) == (-1,0,0)
    assert cross_prod(0,0,1,1,0,0) == (0,1,0)
    assert cross_prod(1,0,0,0,0,1) == (0,-1,0)
def mag(x,y,z):
    return sqrt(x**2+y**2+z**2)

C_x, C_y, C_z = cross_prod(i_x, i_y, i_z, r_x, r_y, r_z)
B_x = C_x * 4*pi*1e-7 /4/pi / mag(r_x,r_y,r_z)**2
B_y = C_y * 4*pi*1e-7 /4/pi / mag(r_x,r_y,r_z)**2
B_z = C_z * 4*pi*1e-7 /4/pi / mag(r_x,r_y,r_z)**2
B = mag(B_x, B_y, B_z)

netB_x, netB_y, netB_z = sum(B_x), sum(B_y), sum(B_z)


test_cross_prod()

def plot_current_vectors():
#    import pdb;pdb.set_trace()
    quiver(wire_segs_x, wire_segs_y, i_x, i_y)

def plot_r_vectors():
#    import pdb;pdb.set_trace()
    quiver(wire_segs_x, wire_segs_y, r_x, r_y)

def plot_B_vectors():
    import pdb;pdb.set_trace()
    quiver(wire_segs_x, wire_segs_z, B_x, B_z)

def plot_wire_loop():
    subplot(2,1,1)
    plot(wire_segs_x,wire_segs_y)
    subplot(2,1,2)
    plot(theta/2/pi,wire_segs_wave_theta/2/pi)

plot_B_vectors()
show()
